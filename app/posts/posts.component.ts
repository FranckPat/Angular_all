import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})

export class PostsComponent implements OnInit {
  postName = [
    {title: 'Mon premier POST'},
    {title: 'Mon deuxième POST'},
    {title: 'Encore un POST'}
  ];
  constructor() {
  }

  ngOnInit() {
  }

}
