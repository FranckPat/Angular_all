import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  i: number;
  x: any;
  z: any;
  y: any;
  @Input() postName: string;
  like = 'LOVE';
  date = new Date();
  dislike = 'DON\'T LOVE';
  constructor() {
    this.i = 0;
    this.y = 0;
    this.x = 0;
    this.z = 0;
  }

  ngOnInit() {
  }
  love(event) {
    this.i++;
    this.y++;
    this.z++;
    this.compteur3(this.z);
    this.compteur(this.y);
    if (this.i > 0) {
      this.goodLove(event);
    } else if (this.i === 0) {
      this.globalLove(event);
    } else {
      this.badLove(event);
    }
  }
  dont_love(event) {
    this.i--;
    this.x++;
    this.compteur2(this.x);
    if (this.i > 0) {
      this.goodLove(event);
    } else if (this.i === 0) {
      this.globalLove(event);
    } else {
      this.badLove(event);
    }
  }
  goodLove(event) {
    console.log(event);
    event.path[3].style.backgroundColor = 'lime';
    event.path[3].children[0].style.color = 'red';
    event.path[3].children[1].style.color = 'white';
    event.path[3].children[0].style.fontSize = '20px';
    event.path[3].children[0].style.textDecoration = 'underline';
    event.path[3].children[1].style.fontSize = '20px';
  }
  badLove(event) {
    event.path[3].style.backgroundColor = 'red';
    event.path[3].children[0].style.color = 'lime';
    event.path[3].children[0].style.textDecoration = 'underline';
    event.path[3].children[0].style.fontSize = '20px';
    event.path[3].children[1].style.color = 'white';
    event.path[3].children[1].style.fontSize = '20px';
  }
  globalLove(event) {
    event.path[3].style.backgroundColor = 'rgb(127.5,127.5,0)';
    event.path[3].children[0].style.color = 'red';
    event.path[3].children[0].style.textDecoration = 'underline';
    event.path[3].children[0].style.fontSize = '20px';
    event.path[3].children[1].style.color = 'white';
    event.path[3].children[1].style.fontSize = '20px';
  }
  compteur(y) {
      if (y >= 0) {
        this.y = y;
      } else if (y > 1) {
        this.y = y + ' LIKES';
      }
  }
  compteur2(x) {
      if (x >= 0) {
        this.x = x;
      } else if (x > 1) {
        this.x = x;
      }
    }
  compteur3(z) {
    if (z >= 0) {
      this.z = z;
    } else if (z > 1) {
      this.z = z;
    }
}
}
